utils = require './utils.cjsx'
React = require 'react'
Isotope = require 'isotope-layout'
$ = require 'jquery'
imagesLoaded = require 'imagesloaded'
imagesLoaded.makeJQueryPlugin $

class UIProducts extends React.Component

  isotope: null

  constructor: (props) ->
    super(props)
    @state =
      products: []
      filters: {}
    @setState = @setState.bind @
    return

  componentDidMount: ->
    @fetchProducts null, =>
      $('.isotope-grid').imagesLoaded =>
        @isotope = new Isotope '.isotope-grid',
          itemSelector: '.isotope-grid-item'
        return
      return
    return

  fetchProducts: (opts, cb) ->
    utils.postJSON '/products/fetch', opts, (response) =>
      if response.status is 'OK'
        @setState
          products: response.products
        , =>
          cb?()
          return
      return
    return

  addFilter: (filter, value) ->
    filters = @state.filters
    if not filters[filter]?
      filters[filter] = {}
    filters[filter] = value
    @setState
      filters: filters
    , =>
      @filterProducts()
    return

  clearFilter: (filter) ->
    if not filter?
      @setState
        filters: {}
      , =>
        @filterProducts()
      return
    filters = @state.filters
    delete filters[filter]
    @setState
      filters: filters
    , =>
      @filterProducts()
    return

  filterProducts: ->
    if @isotope?
      @isotope.arrange
        filter: (itemElem) =>
          shouldRemoveItem = false
          for filter, value of @state.filters
            attr = $(itemElem).data(filter)
            if not attr? or attr.trim().length is 0
              shouldRemoveItem = true
            else
              shouldRemoveItem = value not in attr.split ','
          return not shouldRemoveItem
    return
  
  render: ->
    <div className="container">
      <div className="row">
        <div className="col-sm-2">
          <a href="javascript:;" onClick={@clearFilter.bind @, null}>All Products</a>
        </div>
        <div className="col-sm-2">
          <a href="javascript:;" onClick={@addFilter.bind @, 'tags', 'fraternities'}>Fraternities Only</a>
        </div>
        <div className="col-sm-2">
          <a href="javascript:;" onClick={@addFilter.bind @, 'tags', 'sororities'}>Sororities Only</a>
        </div>
      </div>
      <div className="row">
        <div className="col-sm-12">
          <div className="isotope-grid" ref="products">
            {
              @state.products.map (product) =>
                <div key={product.id} className="isotope-grid-item" data-tags={product.tags}>
                  <img src={product.images[0].src} />
                </div>
            }
          </div>
        </div>
      </div>
    </div>

module.exports =

  UIProducts: UIProducts
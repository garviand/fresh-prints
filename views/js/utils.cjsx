$ = require 'jquery'

module.exports =

  postJSON: (url, params, cb) ->
    start = new Date()
    xhr = $.ajax url,
      data: JSON.stringify params
      contentType: 'application/json'
      type: 'POST'
    xhr.done (data) ->
      cb? data
      return
    xhr.fail (_xhr) ->
      res =
        status: 'FAIL'
        code: _xhr.status
      cb? res
      return
    return
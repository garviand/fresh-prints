###
Shopify client
###

Q = require 'q'
shopifyAPI = require 'shopify-node-api'

Shopify = new shopifyAPI
  shop: process.env.SHOPIFY_STORE_NAME
  shopify_api_key: process.env.SHOPIFY_API_KEY
  access_token: process.env.SHOPIFY_API_SECRET
  verbose: false

module.exports = 

  fetch_products: (opts) ->
    deferred = Q.defer()
    url = '/admin/products.json?'
    options = []
    if opts.type?
      options.push "product_type=#{opts.type}"
    if opts.collectionId?
      options.push "collection_id=#{opts.collectionId}"
    if opts.page?
      options.push "page=#{opts.page}"
    if opts.fields?
      options.push "fields=#{opts.fields.join ','}"
    url = "#{url}#{options.join '&'}"
    Shopify.get url, (err, data, headers) ->
      deferred.resolve data
      return
    return deferred.promise

  fetch_collections: () ->
    deferred = Q.defer()
    Shopify.get '/admin/collection_listings.json', (err, data, headers) ->
      deferred.resolve data
      return
    return deferred.promise


path = require 'path'

koa = require 'koa'
formidable = require 'koa-formidable'
mount = require 'koa-mount'
serve = require 'koa-static'
Pug = require 'koa-pug'

urls = require './urls'

APP_PORT = 3000

app = new koa()

# Body parser
app.use formidable()

pug = new Pug
  viewPath: path.join __dirname, '/../views/templates/'
  debug: false
  pretty: false
  compileDebug: false
  locals:
    staticPrefix: '/static/'
  helperPath: [
    _: require 'lodash'
  ]
  app: app

app.use mount '/static/', serve path.join __dirname, '/../static/'

app.use (next) ->
  start = new Date()
  yield next
  ms = new Date() - start
  console.log "#{@method} #{@url} - #{ms}"
  return

# Url dispatcher
router = urls()
app.use router.routes()

app.listen APP_PORT

console.log "Listening on port #{APP_PORT}"

shopify = require '../apis/shopify'

module.exports =

  fetch: ->
    result = yield shopify.fetch_products
      fields: [
        'id'
        'body_html'
        'images'
        'product_type'
        'tags'
        'title'
        'vendor'
      ]
      type: @request.body.type
      collectionId: @request.body.collection
      page: @request.body.page
    if result.products?
      @body =
        status: 'OK'
        products: result.products
      return
    @body =
      status: 'FAIL'
      message: 'Could not fetch products'
    return
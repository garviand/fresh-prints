gulp = require 'gulp'
nodemon = require 'gulp-nodemon'
env = require 'gulp-env'
less = require 'gulp-less'
webpack = require 'webpack'

path = require 'path'

gulp.task 'dev', ['less', 'webpack', 'start'], ->
  gulp.watch ['./views/less/**/*.less'], ['less']
  gulp.watch ['./views/js/**/*.cjsx'], ['webpack']
  return

gulp.task 'start', (cb) ->
  env
    file: 'envs.json'
    vars:
      NO_DEPRECATION: 'koa'
  options =
    watch: [path.join(__dirname, 'src/'), path.join(__dirname, 'views/templates/')]
    ext: 'coffee pug'
    script: 'src/server.coffee',
  nodemon options
  cb()
  return

gulp.task 'less', ->
  gulp.src('./views/less/**/*.less')
    .pipe less
      paths: [ path.join(__dirname, 'less', 'includes') ]
    .pipe gulp.dest './static/css'
  return

gulp.task 'webpack', (cb) ->
  webpackConfig =
    entry: path.resolve __dirname, 'views/js/app.cjsx'
    output:
      path: path.resolve __dirname, 'static/js'
      filename: 'bundle.js'
      publicPath: path.resolve __dirname, 'static/js'
    module:
      rules: [
        test: /\.cjsx$/
        use: ['coffee-loader', 'cjsx-loader']
      ,
        test: /\.coffee$/
        use: ['coffee-loader']
      ]
    resolve:
      alias:
        'masonry': 'masonry-layout'
        'isotope': 'isotope-layout'

  webpack webpackConfig, (err, stats) ->
    if err?
      console.log err
      return
    cb()

  return